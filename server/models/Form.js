const mongoose = require('mongoose');

const FormSchema = new mongoose.Schema({
    loanAmount: String,
    yearOfStart: String,
    revenues: String,
    monthlyDeposits: String,
    ownership: String,
    ein: String,
    ssn: String,
    dob: String,
    own: Boolean,
    leasePremises: Boolean,
    shortTerm: Boolean
});

module.exports = mongoose.model('Form', FormSchema);