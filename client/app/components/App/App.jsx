import React, { Component } from 'react';
import '../../styles/layout/app.scss';

const App = ({ children }) => (
    <div className="app-layout">
      {children}
    </div>
);

export default App;
