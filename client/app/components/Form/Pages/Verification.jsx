import React, { Component } from 'react';
import { Input, Button } from 'react-materialize';

class VerificationPage extends Component {

    state = {
        formStage: {
            ein: "",
            ssn: "",
            dob: "",
            own: false,
            leasePremises: false,
            shortTerm: false
        },
        triedToSubmit: false
    }

    render() {
        return (
            <div>
                <h4>Congratulations!</h4>
                <hr />
                <h6>Based on the information you provided you could get funded</h6>
                <br/>
                <h6>Please, verify your information</h6>
                <Input
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, ein: event.target.value } }) }}
                    value={this.state.formStage.ein}
                    s={12}
                    label= "Business Tax ID (EIN) (check top of your tax return)"
                    error={this.state.triedToSubmit && !this.state.formStage.ein ? " " : null}
                />
                <Input
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, ssn: event.target.value } }) }}
                    value={this.state.formStage.ssn}
                    s={12}
                    label="SSN (required for verification)"
                    error={this.state.triedToSubmit && !this.state.formStage.ssn ? " " : null}
                />
                <Input
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, dob: event.target.value } }) }}
                    value={this.state.formStage.dob}
                    s={12}
                    label= "DOB (required for soft pull of your credit)"
                    error={this.state.triedToSubmit && !this.state.formStage.dob ? " " : null}
                />

                <Button className="red">Submit</Button>
            </div>
        )
    }
}

export default VerificationPage;