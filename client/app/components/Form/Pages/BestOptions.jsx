import React, { Component } from 'react';
import { Input, Button } from 'react-materialize';

class BestOptions extends Component {

    state = {
        formStage: {
            loanAmount: "",
            yearOfStart: "",
            revenues: "",
            monthlyDeposits: "",
            ownership: ""
        },
        triedToSubmit: false
    };

    yearRegex = /^((0[0-9])|(1[1-2]))\/((1[8-9][0-9][0-9])|(20(0([0-9])|(1[0-8]))))$/;

    constructor(props) {
        super(props);
        this.tryToSubmit = this.tryToSubmit.bind(this);
    }

    tryToSubmit() {
        if(this.state.formStage.loanAmount.length && this.yearRegex.test(this.state.formStage.yearOfStart) && this.state.formStage.revenues.length
            && this.state.formStage.monthlyDeposits.length && this.state.formStage.ownership.length) 
            {
                this.props.passFields(this.state.formStage);
                this.props.sliderRef.current.slickNext();
            };
        console.log(this.state.formStage);
        this.setState({triedToSubmit: true});
    }

    render() {
        let yearError = null;
        
        if (this.state.triedToSubmit && !this.yearRegex.test(this.state.formStage.yearOfStart)) {
            yearError = " ";
        }

        return (
            <div>
                <h4>Financing</h4>
                <hr />
                <h6>By invitation only</h6>
                <h6>Let's find your best financing options</h6>
                <h6>Note: Grab a PDF of 3-months bank statements for fastest processing</h6>
                <Input 
                    type="number"
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, loanAmount: event.target.value }})}}
                    value={this.state.formStage.loanAmount}
                    s={12}
                    label="Requested Loan Amount"
                    error={this.state.triedToSubmit && !this.state.formStage.loanAmount ? " " : null}
                />
                <Input 
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, yearOfStart: event.target.value } })}}
                    value={this.state.formStage.yearOfStart}
                    s={yearError ? 6 : 12}
                    label="Year business started (MM/YYYY)"
                    error={yearError}
                />
                <Input 
                    type="number"
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, revenues: event.target.value } }) }}
                    value={this.state.formStage.revenues}
                    s={12}
                    label="Annual Revenues (approx.)"
                    error={this.state.triedToSubmit && !this.state.formStage.loanAmount ? " " : null}
                />
                <Input 
                    type="number" 
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, monthlyDeposits: event.target.value } }) }}
                    value={this.state.formStage.monthlyDeposits}
                    s={12} 
                    label="Gross Monthly deposits"
                    error={this.state.triedToSubmit && !this.state.formStage.monthlyDeposits ? " " : null}
                />
                <Input 
                    type="number"
                    onChange={(event) => { this.setState({ formStage: { ...this.state.formStage, ownership: event.target.value } }) }}
                    value={this.state.formStage.ownership}
                    s={12} 
                    label="Applicant ownership in business, %"
                    error={this.state.triedToSubmit && !this.state.formStage.ownership ? " " : null}
                />
                <br />
                <Button onClick={this.tryToSubmit} className="red">Next</Button>
            </div>
        )
    }
}

export default BestOptions;