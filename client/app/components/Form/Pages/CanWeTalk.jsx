import React, { Component } from 'react';
import { Input, Button } from 'react-materialize';

class CanWeTalkPage extends Component {


    render() {
        return (
            <div>
                <div className="form-card-text-block">
                    <h5>Thank you!</h5>
                    <hr/>
                </div>
                <div className="form-card-text-block">
                    <h5>One of our funding specialists will call in 10 minutes.</h5>
                    <h5>Can't talk now? Choose the best time to call you.</h5>
                </div>
                <br />
            </div>
        )
    }
}

export default CanWeTalkPage;