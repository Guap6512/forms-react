import React, { Component } from 'react';
import { Input, Button } from 'react-materialize';

class InvitationPage extends Component {


    render() {
        return (
            <div>
                <h4>Financing</h4>
                <hr/>
                <div className="form-card-text-block">
                    <h5>By invitation only</h5>
                </div>
                <div className="form-card-text-block">
                    <h5>Members save up to $100 projected savings based on previous promotional offers. Subject to underwriting review.</h5>
                </div>
                <br/>
                <Button onClick={() => {this.props.sliderRef.current.slickNext()}} className="red">Please, confirm your invitation</Button>
            </div>
        )
    }
}

export default InvitationPage;