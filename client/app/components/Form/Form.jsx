import React, { Component } from 'react';
import { Card, Button, CardTitle } from 'react-materialize';

import Slider from 'react-slick';

import '../../styles/layout/form.scss';

import InvitationPage from './Pages/InvitationPage';
import BestOptions from './Pages/BestOptions';
import Verification from './Pages/Verification';
import CanWeTalk from './Pages/CanWeTalk';

class Form extends Component {
  constructor(props) {
    super(props);
    this.passFields = this.passFields.bind(this);
    this.slider = React.createRef();
    this.state = {
        form: {
          loanAmount: "",
          yearOfStart: "",
          revenues: "",
          monthlyDeposits: "",
          ownership: "",
          ein: "",
          ssn: "",
          dob: "",
          own: false,
          leasePremises: false,
          shortTerm: false
        }
    };
  }

  passFields(object) {
    let newForm = {...this.state.form, ...object};
    this.setState({form: newForm});
  }

  componentDidMount() {

  }

  render() {
    let sliderSettings = {
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      infinite: false,
    };

    return (
        <div className="form-card-container">
          <Slider {...sliderSettings} ref={this.slider}>
            <Card className="form-card">
              <InvitationPage sliderRef={this.slider}/>
            </Card>
            <Card className="form-card">
              <BestOptions sliderRef={this.slider} passFields={this.passFields} form={this.state.form}/>
            </Card>
            <Card className="form-card">
              <Verification sliderRef={this.slider} />
            </Card>
            <Card className="form-card">
              <CanWeTalk />
            </Card>
          </Slider>
        </div>
      );
  }
}

export default Form;
